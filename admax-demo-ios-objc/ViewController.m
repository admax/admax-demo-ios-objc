@import GoogleMobileAds;
@import AdmaxPrebidMobile;

#import "ViewController.h"
#import "DemoAdmaxExceptionLogger.h"

@interface ViewController () <GADBannerViewDelegate, GADAppEventDelegate, AdSizeDelegate>
    @property (weak, nonatomic) IBOutlet UIView *bannerView;
    @property (nonatomic, strong) GAMBannerView *dfpView;
    @property (nonatomic, strong) GAMInterstitialAd *dfpInterstitial;
    @property (nonatomic, strong) GAMRequest *request;
    @property (nonatomic, strong) BannerAdUnit *bannerUnit;
    @property (nonatomic, strong) InterstitialAdUnit *interstitialUnit;
    
    @end

@implementation ViewController
    
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([self.adUnit isEqualToString:@"Banner"])
    [self loadDFPBanner];
    if([self.adUnit isEqualToString:@"Interstitial"])
    [self loadDFPInterstitial];
}
    
-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.bannerUnit stopAutoRefresh];
}
    
    
    
-(void) loadDFPBanner {
    if ([self.adBidder isEqualToString:@"Xandr"]) {
        self.bannerUnit = [[BannerAdUnit alloc] initWithConfigId:@"dbe12cc3-b986-4b92-8ddb-221b0eb302ef" size:CGSizeMake(320, 50) viewController:self adContainer:self.bannerView];
    } else if ([self.adBidder isEqualToString:@"Criteo"]) {
        self.bannerUnit = [[BannerAdUnit alloc] initWithConfigId:@"fb5fac4a-1910-4d3e-8a93-7bdbf6144312" size:CGSizeMake(320, 50) viewController:self adContainer:self.bannerView];
    } else if ([self.adBidder isEqualToString:@"Smart"]) {
        self.bannerUnit = [[BannerAdUnit alloc] initWithConfigId:@"fe7d0514-530c-4fb3-9a52-c91e7c426ba6" size:CGSizeMake(320, 50) viewController:self adContainer:self.bannerView];
    }
//    [self.bannerUnit addAdditionalSizeWithSizes: @[[NSValue valueWithCGSize:CGSizeMake(300, 250)]]];
//    [self.bannerUnit setAutoRefreshMillisWithTime:35000];
    self.bannerUnit.adSizeDelegate = self;
    self.dfpView = [[GAMBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    self.dfpView.rootViewController = self;
    self.dfpView.adUnitID = @"/21807464892/pb_admax_320x50_top";
    self.dfpView.delegate = self;
    self.dfpView.appEventDelegate = self;
    [self.bannerView addSubview:self.dfpView];
    self.request = [[GAMRequest alloc] init];
    
    __weak ViewController *weakSelf = self;
    [self.bannerUnit fetchDemandWithAdObject:self.request completion:^(enum ResultCode result) {
        NSLog(@"Prebid demand result %ld", (long)result);
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.dfpView loadRequest:weakSelf.request];
        });
    }];
}
    
-(void) loadDFPInterstitial {
    if ([self.adBidder isEqualToString:@"Xandr"]) {
        self.interstitialUnit = [[InterstitialAdUnit alloc] initWithConfigId:@"dbe12cc3-b986-4b92-8ddb-221b0eb302ef" viewController:self];
    } else if ([self.adBidder isEqualToString:@"Criteo"]) {
        self.interstitialUnit = [[InterstitialAdUnit alloc] initWithConfigId:@"5ba30daf-85c5-471c-93b5-5637f3035149" viewController:self];
    } else if ([self.adBidder isEqualToString:@"Smart"]) {
        self.interstitialUnit = [[InterstitialAdUnit alloc] initWithConfigId:@"2cd143f6-bb9d-4ca9-9c4b-acb527657177" viewController:self];
    }
    self.request = [[GAMRequest alloc] init];
    __weak ViewController *weakSelf = self;
    [self.interstitialUnit fetchDemandWithAdObject:self.request completion:^(enum ResultCode result) {
        NSLog(@"Prebid demand result %ld", (long)result);
        [GAMInterstitialAd loadWithAdManagerAdUnitID:@"/21807464892/pb_admax_interstitial" request:weakSelf.request completionHandler:^(GAMInterstitialAd * _Nullable interstitialAd, NSError * _Nullable error) {
            if (error) {
                NSLog(@"Failed to load interstitial ad with error: %@", [error localizedDescription]);
            } else {
                weakSelf.dfpInterstitial = interstitialAd;
                weakSelf.dfpInterstitial.appEventDelegate = self;
                [[Utils shared] findPrebidCreativeBidder:interstitialAd success:^(NSString* bidder) {
                    NSLog(@"SDK bidder rendering: %@", bidder);
                } failure:^(NSError * _Nonnull error) {
                    NSLog(@"Ad Server SDK rendering");
                    [weakSelf.dfpInterstitial presentFromRootViewController:weakSelf];
                }];
            }
        }];
    }];
}

-(void) onAdLoadedWithAdUnit:(AdUnit *)adUnit size:(CGSize)size adContainer:(UIView *)adContainer {
    NSLog(@"ADMAX onAdLoaded with Size: %fx%f", size.width, size.height);
}
    
#pragma mark :- DFP delegates
-(void) bannerViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"Ad received");
    [[Utils shared] findPrebidCreativeSize:bannerView
                                   success:^(CGSize size) {
                                       if ([bannerView isKindOfClass:[GAMBannerView class]]) {
                                           GAMBannerView *dfpBannerView = (GAMBannerView *)bannerView;
                                           [dfpBannerView resize:GADAdSizeFromCGSize(size)];
                                       }
                                   } failure:^(NSError * _Nonnull error) {
                                       NSLog(@"error: %@", error.localizedDescription);
                                   }];
}
    
- (void)bannerView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}

-(void) adView:(GADBannerView *)banner didReceiveAppEvent:(NSString *)name withInfo:(NSString *)info
{
    self.bannerUnit.isGoogleAdServerAd = false;
    if (![self.bannerUnit isAdServerSdkRendering]) {
        [self.bannerUnit loadAd];
    }
}

-(void) interstitialAd:(GADInterstitialAd *)interstitial didReceiveAppEvent:(NSString *)name withInfo:(NSString *)info
{
    self.interstitialUnit.isGoogleAdServerAd = false;
    if (![self.interstitialUnit isAdServerSdkRendering]) {
        [self.interstitialUnit loadAd];
    } else {
        [self.dfpInterstitial presentFromRootViewController:self];
    }
}

@end
