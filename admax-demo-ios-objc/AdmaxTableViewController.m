@import GoogleMobileAds;
@import AdmaxPrebidMobile;

#import "AdmaxTableViewController.h"

@interface AdmaxTableViewController () <GADBannerViewDelegate, GADAppEventDelegate, GADAdSizeDelegate, AdSizeDelegate>
    @property (nonatomic, strong) GAMBannerView *dfpView;
    @property (nonatomic, strong) GAMRequest *request;
    @property (nonatomic, strong) BannerAdUnit *bannerUnit;
    
@end

@implementation AdmaxTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdmaxCell" forIndexPath:indexPath];
    [self loadDFPBanner:cell.contentView];
    return cell;
}

-(void) loadDFPBanner:(UIView *) contentView {
    
    self.bannerUnit = [[BannerAdUnit alloc] initWithConfigId:@"dbe12cc3-b986-4b92-8ddb-221b0eb302ef" size:CGSizeMake(300, 250) viewController:self adContainer:contentView];
    self.bannerUnit.adSizeDelegate = self;
//    [self.bannerUnit setAutoRefreshMillisWithTime:30000];
    self.dfpView = [[GAMBannerView alloc] initWithAdSize:kGADAdSizeFluid];
    self.dfpView.validAdSizes = @[NSValueFromGADAdSize(kGADAdSizeFluid), NSValueFromGADAdSize(kGADAdSizeMediumRectangle)];
    self.dfpView.rootViewController = self;
    self.dfpView.adUnitID = @"/21807464892/pb_admax_300x250_top";
    self.dfpView.delegate = self;
    self.dfpView.adSizeDelegate = self;
    self.dfpView.appEventDelegate = self;
    self.dfpView.translatesAutoresizingMaskIntoConstraints = NO;
    [contentView addSubview:self.dfpView];
    [self.dfpView.centerXAnchor constraintEqualToAnchor:contentView.centerXAnchor].active = YES;
    [self.dfpView.centerYAnchor constraintEqualToAnchor:contentView.centerYAnchor].active = YES;
    self.request = [[GAMRequest alloc] init];
    
    __weak AdmaxTableViewController *weakSelf = self;
    [self.bannerUnit fetchDemandWithAdObject:self.request completion:^(enum ResultCode result) {
        NSLog(@"Prebid demand result %ld", (long)result);
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.dfpView loadRequest:weakSelf.request];
        });
    }];
}

-(void) onAdLoadedWithAdUnit:(AdUnit *)adUnit size:(CGSize)size adContainer:(UIView *)adContainer {
    NSLog(@"ADMAX onAdLoaded with Size: %fx%f", size.width, size.height);
}

#pragma mark :- DFP delegates
-(void) bannerViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"Ad received");
    [[Utils shared] findPrebidCreativeSize:bannerView
                                   success:^(CGSize size) {
                                       if ([bannerView isKindOfClass:[GAMBannerView class]]) {
                                           GAMBannerView *dfpBannerView = (GAMBannerView *)bannerView;
                                           [dfpBannerView resize:GADAdSizeFromCGSize(size)];
                                       }
                                   } failure:^(NSError * _Nonnull error) {
                                       NSLog(@"error: %@", error.localizedDescription);
                                   }];
}
    
- (void)bannerView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"adView:didFailToReceiveAdWithError: %@", error.localizedDescription);
}

-(void) adView:(GADBannerView *)banner didReceiveAppEvent:(NSString *)name withInfo:(NSString *)info
{
    self.bannerUnit.isGoogleAdServerAd = false;
    if (![self.bannerUnit isAdServerSdkRendering]) {
        [self.bannerUnit loadAd];
    }
}

-(void) adView:(GADBannerView *)bannerView willChangeAdSizeTo:(GADAdSize)size {

}

@end
