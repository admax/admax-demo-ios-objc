#import "PrebidNavigationController.h"
#import "ViewController.h"

@interface PrebidNavigationController ()

@property (nonatomic, strong) NSArray *adBidderList;
@property (nonatomic, strong) NSArray *adUnitList;

@end

@implementation PrebidNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Admax Demo";
    
    self.title = @"Admax Demo";
    
    self.adBidderList = @[@"Xandr", @"Criteo", @"Smart"];
    self.adUnitList = @[@"Banner", @"Interstitial"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.adBidderList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.adUnitList.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self.adBidderList objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *adUnit = [self.adUnitList objectAtIndex:indexPath.row];
    cell.textLabel.text = adUnit;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
//    if (indexPath.row == 2) {
//        ViewController *tableViewController = [storyboard instantiateViewControllerWithIdentifier:@"tableViewController"];
//        [self.navigationController pushViewController:tableViewController animated:YES];
//    } else {
        ViewController * viewController = [storyboard instantiateViewControllerWithIdentifier:@"viewController"];
        viewController.adBidder = [self.adBidderList objectAtIndex:indexPath.section];
        viewController.adUnit = [self.adUnitList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:viewController animated:YES];
//    }
}
@end
